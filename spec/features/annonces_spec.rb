require 'rails_helper'

RSpec.feature "Annonces", type: :feature do
  scenario "allow a user to add an annonce" do

    visit new_annonce_path
    select "Produit" or "Service" , :from => "Product"
    fill_in "Product_name", with: "mac"
    fill_in "Product_type", with: "ordinateur"
    fill_in "description_annonce", with: "mac neuf"

    click.on("Je valide")

    except(annonce_path).to have_content("Product")
    except(annonce_path).to have_content("Product_name")
    except(annonce_path).to have_content("Product_type")
    except(annonce_path).to have_content("description_annonce")

  end
end
