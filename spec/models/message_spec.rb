require 'rails_helper'

RSpec.describe Message, type: :model do
    describe "validation" do 
        it { should validate_presence_of(:content) }
        it { should validate_presence_of(:conversation_id) }  
        it { should validate_presence_of(:user_id) } 
    end

    describe "association" do 
        #shoulda_matchers simplifie l'ecriture
          it { should belong_to(:user) }
          it { should belong_to(:conversation) }
    end
end