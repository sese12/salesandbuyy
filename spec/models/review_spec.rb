require 'rails_helper'

RSpec.describe Review, type: :model do
  describe "association" do 
    it { should belong_to(:user) }
    it { should belong_to(:annonce) }
  end
end
