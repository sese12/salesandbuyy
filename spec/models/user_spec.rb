require 'rails_helper'

RSpec.feature "Users", type: :feature do
  describe "validations" do 
    it "requires lastname" do 
      user = User.new(lastname: '')
      expect(user.valid?).to be_falsy
    end 
    it "requires firstname" do 
      user = User.new(firstname: '')
      expect(user.valid?).to be_falsy
    end 
    it "requires poste" do 
      user = User.new(poste: '')
      expect(user.valid?).to be_falsy
    end 
    it "requires firm_name" do 
      user = User.new(lastname: '')
      expect(user.valid?).to be_falsy
    end 
    it "requires firm_sector" do 
      user = User.new(lastname: '')
      expect(user.valid?).to be_falsy
    end 
    it "requires address" do 
      user = User.new(address: '')
      expect(user.valid?).to be_falsy
    end 
  end
  
  describe "association" do
    it { have_many(:annonces) }
    it { have_many(:negoces) }
    it { have_many(:reviews) }
  end

end
