require 'rails_helper'

RSpec.describe Annonce, type: :model do
  before(:all) do
    user = FactoryBot.create(:user)
  end

  describe "validations" do 
    it "requires to choose product or service" do 
      annonce = Annonce.new
    end 

    it "requires product_type" do 
      annonce = Annonce.new(product_type: '')
      annonce.valid?
      expect(annonce.errors[:product_type]).not_to be_empty
    end 

    it "requires description_annonce" do 
      annonce = Annonce.new(description_annonce: '')
      annonce.valid?
      expect(annonce.errors[:description_annonce]).not_to be_empty
    end 
     
    it "belongs to user" do 
      annonce = Annonce.new(product_type:'', product_name:'', description_annonce: '', user: nil)
      expect(annonce.valid?).to be_falsy
    end 

    it "has belongs_to user association" do 
      user = FactoryBot.create(:user)
      annonce= FactoryBot.create(:annonce, user: user)
      expect(annonce.user).to eq(user)
    end
    
    describe "association" do 
    #shoulda_matchers simplifie l'ecriture
      it { should belong_to(:user) }
      it { should have_many(:photos) }
      it { should have_many(:negoces) }
      it { should have_many(:reviews) }
    end

end
end
