FactoryBot.define do
    factory :annonce do
        user
        product "produit"
        product_name "mac"
        product_type "ordinateur"
        description_annonce "mac neuf"
    end
end
