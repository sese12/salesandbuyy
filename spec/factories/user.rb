FactoryBot.define do
  factory :user do
    lastname "Sese"
    firstname { Faker::Name.first_name }
    firm_name "Sese"
    firm_sector "informatique"
    poste "boss"
    address "1 rue du Paradis"
    email { Faker::Internet.email }
    password "sesese"
  end
end
