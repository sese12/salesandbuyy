require 'rails_helper'

RSpec.describe AnnoncesController, type: :controller do
    describe "GET#index" do     
    end

    describe "GET#new" do
        it "renders :new annonce" do 
            get :new
            except(response).to render_annonce(:new)
        end
        it "assigns new Annonce to @annonce" do
        get :new 
        expect(assigns(:annonce)).to be_a_new(Annonce)
        end
    end

    describe "POST#create" do
        context "data is valid" do 
        it "redirects to annonces#show" do 
         annonce = FactoryBot.attributes_for(:annonce)
            expect(response).to redirect_to(annonce_path(assigns[:annonce]))
        end
   
        it "creates new annonce in database" do
            expect {
                 annonce = FactoryBot.attributes_for(:annonce)
            }.to change(Annonce, :count).by(1)
        end
        end

        context "data is invalid" do 
        it "renders :new template" do 
             annonce = FactoryBot.attributes_for(:annonce)
            expect(response).to render_template(:new) 
        end
        it "doesn't create new annonce in the database" do
        expect {
             annonce = FactoryBot.attributes_for(:annonce)
        }.not_to change(Annonce, :count)
        end
        end   
    end

    describe "GET#show" do 
        let(:annonce) {FactoryBot.create(:annonce)}
        it "renders :show annonce" do 
            get :show, id: annonce.id
            expect(response).to render_annonce(:show)
        end
        it "assigns requested annonce to @annonce" do 
            get :show, id: annonce.id
            expect(assigns(:annonce)).to eq(annonce)
        end
    end
end
