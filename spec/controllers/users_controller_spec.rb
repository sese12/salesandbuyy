require 'rails_helper'

RSpec.describe UsersController, type: :controller do
	describe "GET#index" do
		context 'ransack search by user' do
			before do
				user = FactoryBot.create(:user, firm_name: 'Sese', firm_sector: 'informatique')
			end
			it "let user search an user" do 
			end

			it "should have a matching firm_name" do
				get :search, q: :user 
				except(@user.firm_name).to eq('Sese')
			end

			it "should have a matching firm_sector" do
				get :search, q: :user 
				except(@user.firm_sector).to eq('informatique')
			end
		end
	end


	describe "GET#show" do 
		context 'user is connected' do
			it {is_expected.to show all users with (params[:id])}
		end
	end
end
