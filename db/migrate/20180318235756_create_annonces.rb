class CreateAnnonces < ActiveRecord::Migration[5.1]
  def change
    create_table :annonces do |t|
      t.string :product
      t.string :product_name
      t.string :product_type
      t.text :description_annonce
      t.integer :price
      t.integer :quantite
      t.boolean :active
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
