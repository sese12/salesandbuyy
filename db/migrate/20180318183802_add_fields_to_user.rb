class AddFieldsToUser < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :lastname, :string
    add_column :users, :firstname, :string
    add_column :users, :poste, :string
    add_column :users, :firm_name, :string
    add_column :users, :firm_sector, :string
    add_column :users, :firm_siret, :string
    add_column :users, :employed_firm, :integer
    add_column :users, :statut_firm, :string
    add_column :users, :description_firm, :text
    add_column :users, :address, :string
    add_column :users, :phone_number, :string
    add_column :users, :fax_number, :string
    add_column :users, :produit, :boolean
    add_column :users, :service, :boolean
    add_column :users, :description_product, :text
    add_column :users, :site_web, :string
  end
end
