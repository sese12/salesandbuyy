class CreateNegoces < ActiveRecord::Migration[5.1]
  def change
    create_table :negoces do |t|
      t.references :user, foreign_key: true
      t.references :annonce, foreign_key: true
      t.integer :quantite_ask
      t.integer :price
      t.integer :total

      t.timestamps
    end
  end
end
