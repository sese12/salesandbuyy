Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  root 'pages#home'
  resources :pages, only: [:home, :index, :new, :create]

  devise_for :users, path: '',
                     path_names: { sign_up: 'insciption', sign_in: 'connexion', sign_out: 'déconnexion', edit: 'profil' },
                     controllers: { registrations: 'registrations',
                                    confirmations: 'confirmations',
                                    omniauth_callbacks: 'omniauth_callbacks' }

  resources :users, only: %i[show index destroy] do
    match :search, to: 'users#index', via: :post, on: :collection
  end

  resources :annonces do
    resources :negoces, only: %i[create]
    resources :reviews, only: %i[create destroy]
  end

  resources :photos

  resources :conversations, only: [:index, :create] do
    resources :messages, only: [:index, :create]
  end

  get '/partenariats' => 'negoces#your_partnerships'
  get '/propositions' => 'negoces#your_negoces'
end
