class AppMailer < ActionMailer::Base
  def new_reservation(annonce, devi)
    @user = User.find(annonce.user_id)
    @devi = devi
    @annonce = annonce

    mail(from: 'Sales&Buy <contact@startme-up.com', to: @user.email, subject: 'Nouvelle annonce !')
  end
end
