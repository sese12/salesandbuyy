class ReviewsController < ApplicationController
  def create
    @review = current_user.reviews.create(review_params)
    redirect_to @review.annonce
  end

  def destroy
    @review = Review.find(params[:id])
    annonce = @review.annonce
    @review.destroy
    redirect_to annonce
  end

  private

  def review_params
    params.require(:review).permit(:comment, :star, :annonce_id)
  end
end
