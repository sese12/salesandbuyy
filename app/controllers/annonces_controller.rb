class AnnoncesController < ApplicationController
  before_action :set_annonce, only: %i[show edit update destroy]
  before_action :authenticate_user!, except: [:show]
  before_action :require_same_user, only: %i[edit update destroy]

  def index
    @annonces = current_user.annonces

    @search = User.ransack params[:q]
    @users = @search.result
  end

  def new
    @annonce = current_user.annonces.build
  end

  def create
    @annonce = current_user.annonces.build(annonce_params)
    if @annonce.save
      if params[:images]
        params[:images].each do |i|
          @annonce.photos.create(image: i)
        end
      end
      @photos = @annonce.photos
      redirect_to annonce_path(@annonce), notice: 'Votre annonce a été ajouté avec succès'
    else
      render :new
    end
  end

  def show
    @photos = @annonce.photos

    @reviews = @annonce.reviews
    if current_user
      # ATTENTION : changer @booked
      # @booked = Negoce.where("annonce_id = ? AND user_id = ?", @annonce.id, current_user.id).present?
      @hasReview = @reviews.find_by(user_id: current_user.id)
    end
  end

  def edit
    @photos = @annonce.photos
  end

  def update
    if @annonce.update(annonce_params)
      if params[:images]
        params[:images].each do |i|
        @annonce.photos.create(image: i)
      end
    end
      @annonce.save
      redirect_to annonce_path(@annonce), notice: 'Modification enregistrée...'
    else
      render :edit
    end
  end

  def destroy
    @annonce.destroy
      if params[:images]
        params[:images].each do |i|
        @annonce.photos.destroy(image:i)
      end
    end
    redirect_to annonces_path, notice: "votre annonce à été suprimmée avec succés "
  end


  private

  def set_annonce
    @annonce = Annonce.find(params[:id])
  end

  def annonce_params
    params.require(:annonce).permit(:product, :product_name, :product_type,
                                    :description_annonce, :price, :quantite, :active)
  end

  def require_same_user
    if current_user.id != @annonce.user_id
      flash[:danger] = "Vous n'avez pas le droit de modifier cette page"
      redirect_to root_path
    end
  end
end
