class NegocesController < ApplicationController
  before_action :authenticate_user!

  def create
    @negoce = current_user.negoces.create(negoce_params)
    redirect_to @negoce.annonce, notice: 'Votre demande de partenariat a été envoyée'
  end

  def your_partnerships
    @partnerships = current_user.negoces
  end

  def your_negoces
    @annonces = current_user.annonces
  end

  private

  def negoce_params
    params.require(:negoce).permit(:price, :total, :annonce_id, :quantite_ask)
  end
end
