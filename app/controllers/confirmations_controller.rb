class ConfirmationsController < Devise::ConfirmationsController
  private

    def after_confirmation_path_for(_resource_name, _resource)
      edit_user_registration_path
    end
end
