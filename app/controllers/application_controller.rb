class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :configure_permitted_parameters, if: :devise_controller?

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: %i[lastname firstname poste firm_name firm_sector address])
    devise_parameter_sanitizer.permit(:account_update, keys: %i[lastname firstname poste firm_sector firm_siret employed_firm statut_firm description_firm address firm_name phone_number fax_number produit service description_product site_web email password password_confirmation avatar])
  end
end
