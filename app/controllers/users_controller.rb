class UsersController < ApplicationController
  before_action :authenticate_user!
  
  def index
    @search = User.search(params[:q])
    @users = @search.result
  end

  def show
    @user = User.find(params[:id])
    @annonces = @user.annonces
  end

end
