class PagesController < ApplicationController
  def home
    @annonces = Annonce.order('RANDOM()').limit(3)
    @page = Page.new
  end

  def create
    @page = Page.new(params[:page])
    @page.request = request
    if @page.deliver
      redirect_to root_path, notice: 'Votre message a été envoyé avec succés'
    else
      redirect_to root_path, alert: 'Veuillez saisir un email valide'
    end
  end
end
