class Annonce < ApplicationRecord
  belongs_to :user
  has_many :photos, dependent: :destroy
  has_many :negoces
  has_many :reviews

  validates :product, presence: true
  validates :product_name, presence: true, length: { maximum: 75 }
  validates :product_type, presence: true
  validates :description_annonce, presence: true, length: { maximum: 600 }

  def average_rating
    reviews.count == 0 ? 0 : reviews.average(:star).round(2)
  end
end
