module ApplicationHelper
  def avatar_url(user)
    if user.image
      user.image
    else # "user.avatar" ou bien "current_user.avatar" ?
      if user.avatar.present?
        user.avatar.url
      else
        'default_image.png'
      end
    end
  end
end